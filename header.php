
<html>
    
            

<div class="wrapper">
<div id="sticky" class="navMenu">
  <div class="menu zoomInDown animated">

        <a href="#planet1link" class="one"onclick="webPlanetNameGraphicDesign()">Graphic Design</a>
        <a href="#planet2link" class="two"onclick="webPlanetNameVideoProduction()">Video Production</a>
        <a href="#planet3link" class="three"onclick="webPlanetNameAnimation()">Animation</a>
        <a href="#planet4link" class="four" onclick="webPlanetNamePhotography()">Photography</a>
        <a href="#planet5link" class="five" onclick="webPlanetNameWebDevelopment()">Web Development</a>
        <a href="#about" class="six">About</a>   
        <div class="oval"></div>
        <div class="circle">
        <li class="line1"><div class="blur1" ><img src="images/graphicDesignBlur.png" height="80px" width="80px"></div><img src="images/graphicDesign.png" height="40px" width="40px"></li>
        <li class="line2"><div class="blur2" ><img src="images/videoProductionBlur.png" height="80px" width="80px"></div><img src="images/videoProduction.png" height="40px" width="40px"></li>
        <li class="line3"><div class="blur3" ><img src="images/animationBlur.png" height="80px" width="80px"></div><img src="images/animation.png" height="40px" width="40px"></li>
        <li class="line4"><div class="blur4" ><img src="images/photographyBlur.png" height="80px" width="80px"></div><img src="images/photography.png" height="40px" width="40px"></li>
        <li class="line5"><div class="blur5" ><img src="images/webDevBlur.png" height="80px" width="80px"></div><img src="images/webDev.png" height="40px" width="40px"></li>
        <li class="line6"><div class="blur6" ><img src="images/genericBlur.png" height="80px" width="80px"></div><img src="images/generic.png" height="40px" width="40px"></li>
        <div class="sun"><img src="images/sun.png" height="40px" width="40px"></div>
        </div>
    </div>
    </div>
           <div id="sticky-anchor"></div>
</div>
    <div id="portfolioDayTitle"><h1>DMACC Portfolio Day 2016</h1></div>
    
    <div class="moon zoomInLeft animated">
    
        <div id="holder"> 
	    <div id="timer">
		<div id="note"></div>
		<div id="countdown">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="day1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="day2">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="day3">
			<img height=21 id="colon1" src="digital-numbers/colon.png" width=9 name="d1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="h1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="h2">
			<img height=21 id="colon2" src="digital-numbers/colon.png" width=9 name="g1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="m1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="m2">
			<img height=21 id="colon3" src="digital-numbers/colon.png" width=9 name="j1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="s1">
			<img height=21 src="digital-numbers/bkgd.gif" width=16 name="s2">
			<div id="title">
				<div class="title" style="position: absolute; top: 36px; left: 42px">DAYS</div>
				<div class="title" style="position: absolute; top: 36px; left: 105px">HRS</div>
				<div class="title" style="position: absolute; top: 36px; left: 156px">MIN</div>
				<div class="title" style="position: absolute; top: 36px; left: 211px">SEC</div>
			</div>
		</div>
	</div>
</div>
</div> 


<div class="mainContent zoomInRight animated">
<div class="dimmed"></div>
 <div class="content">
    <h3>Event Info</h3>
    <ul style="text-align:center; list-style-type:none; padding:10px;">
        <li>Greater Des Moines Botanical Garden</li>
        <li>909 Robert D. Ray Dr, Des Miones, IA 50309</li>
        <br>
        <li>Thursday April 28th, 2016</li>
        <li>11AM - 5PM</li>
        <br>
        <li>Free Admission</li>
        <li><a href="https://www.google.com/maps/dir/''/iowa+botanical+garden/@41.5966877,-93.6842279,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x87ee99a457093b8d:0x2e0ddf50113251e7!2m2!1d-93.614188!2d41.596709" style="color:white;" target="_blank" alt="Map">Click Here for Map</a></li>
    </ul>
  </div>
</div>
</html>

