<html>
<head>
    
<script type="text/javascript">		

</script>
    <title>Portfolio Day</title>
    <script type="text/javascript" src="jquery-1.11.3.min.js"></script>
    <script  type="text/javascript" src="script.js"></script>
    <script  type="text/javascript" src="wow.min.js"></script>
    <script src="prefixfree.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="animate.css">

    
    <script>
    
        new WOW().init();
    
    </script>
</head>

<body onload="countdown(year,month,day,hour,minute)">

    <header>
             <?php include 'header.php';?>
    </header> 
     
    <div id="main" class="wow fadeInLeft">
        
        <?php include 'galaxy.php';?>
    </div>
    
    <footer>
        <?php include 'footer.php';?>
    </footer>
    <div id="fade" class="black_overlay"></div>
</body>
</html>